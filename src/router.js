import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Users from './views/Users.vue'
import UserProfile from './views/UserProfile.vue'
import UserEdit from './views/UserEdit.vue'
import UserAdd from './views/UserAdd.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/user',
      name: 'users',
      component: Users
    },
    {
      path: '/user/add',
      name: 'addUser',
      component: UserAdd
    },
    {
      path: '/user/:id',
      component: UserProfile
    },
    {
      path: '/user/:id/edit',
      component: UserEdit
    }
  ]
})
