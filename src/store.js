import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const initState = {
  people: [
    {
      id: 1,
      name: 'Владимир',
      surname: 'Путин',
      patronymic: 'Владимирович',
      birthdate: '01/02/2015',
      gender: 'мужской',
      address: 'Арбат, 123, кв. 101'
    },
    {
      id: 2,
      name: 'Нурсултан',
      surname: 'Назарбаев',
      patronymic: 'Абишевич',
      birthdate: '01/02/2010',
      gender: 'мужской',
      address: 'Фурманова, 13, кв. 999'
    },
    {
      id: 3,
      name: 'Алмазбек',
      surname: 'Атамбаев',
      patronymic: 'Шаршенович',
      birthdate: '01/02/2000',
      gender: 'мужской',
      address: 'Главная, 10, кв. 111'
    },
    {
      id: 4,
      name: 'Александр',
      surname: 'Лукашенко',
      patronymic: 'Нурсултанович',
      birthdate: '01/02/1900',
      gender: 'мужской',
      address: 'Белорусская, 8, кв. 1'
    }
  ],
  documents: [
    {
      id: 1,
      userId: 1,
      number: 123000123,
      type: 'Удостоверение',
      issuer: 'МВД',
      dateOfIssue: '01/01/2000',
      expireDate: '02/05/2002'
    },
    {
      id: 2,
      userId: 2,
      number: 122000122,
      type: 'Паспорт',
      issuer: 'МЮ',
      dateOfIssue: '01/01/2000',
      expireDate: '02/05/2002'
    },
    {
      id: 3,
      userId: 3,
      number: 101000101,
      type: 'Паспорт',
      issuer: 'МЮ',
      dateOfIssue: '01/01/1988',
      expireDate: '02/05/2000'
    }
  ]
}

export default new Vuex.Store({
  state: initState,
  mutations: {
    saveUser (state, payload) {
      state.people.push(payload)
    },
    removeUser (state, id) {
      const index = state.people.map(p => p.id).indexOf(id)
      if (index !== -1) {
        state.people.splice(index, 1);
      }
    },
    reset (state) {
      Object.assign(state, initState);
    }
  },
  getters: {
    getUserById: (state) => (id) => {
      return state.people.filter(p => p.id == id)
    }
  },
  actions: {
    saveUser ({commit}, payload) {
      commit('saveUser', payload)
    },
    removeUser ({commit}, {id}) {
      commit('removeUser', id)
    }
  }
})
